# Insurance Data Set

The aim is to predict a Doctor's Consultation Fees.
https://www.kaggle.com/nitin194/doctor-fees-prediction

## Attribute Information:

- age integer
- sex binary
- bmi (body mass index) float
- children integer
- smoker binary
- region categorial
- charges float

## Source:

This Dataset has been taken from Hackathon.

## Relevant Papers:

1. H. T. Kahraman, Sagiroglu, S., Colak, I., Developing intuitive knowledge classifier and modeling of users' domain dependent data in web,
   Knowledge Based Systems, vol. 37, pp. 283-295, 2013.
2. Kahraman, H. T. (2009). Designing and Application of Web-Based Adaptive Intelligent Education System. Gazi University Ph. D. Thesis, Turkey, 1-156.
