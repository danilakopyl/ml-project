# User Knowledge Modeling Data Set

It is the real dataset about the students' knowledge status about the subject of Electrical DC Machines. The dataset had been obtained from Ph.D. Thesis.
https://archive.ics.uci.edu/ml/datasets/User+Knowledge+Modeling

## Attribute Information:

- **STG** (The degree of study time for goal object materails) float
- **SCG** (The degree of repetition number of user for goal object materails) float
- **STR** (The degree of study time of user for related objects with goal object) float
- **LPR**(The exam performance of user for related objects with goal object) float
- **PEG** (The exam performance of user for goal objects) float
- **UNS** (The knowledge level of user) category

## Source:

1. Institution: _Faculty of Technology, Department of Software Engineering, Karadeniz Technical University, Trabzon, Turkiye_
2. Institution: _Faculty of Technology, Department of Electrical and Electronics Engineering, Gazi University, Ankara, Turkiye_
3. Institution:_Faculty of Technology, Department of Computer Engineering, Gazi University, Ankara, Turkiye_

Donor: undergraduate students of Department of Electrical Education of Gazi University in the 2009 semester
Date: October, 2009

## Relevant Papers:

1. H. T. Kahraman, Sagiroglu, S., Colak, I., Developing intuitive knowledge classifier and modeling of users' domain dependent data in web,
   Knowledge Based Systems, vol. 37, pp. 283-295, 2013.
2. Kahraman, H. T. (2009). Designing and Application of Web-Based Adaptive Intelligent Education System. Gazi University Ph. D. Thesis, Turkey, 1-156.
