# Insurance Data Set

Data contain information of the people and based on this how much insurance company charge to insure them.
Objective is to predict the insurance charges for the new people based on the information we will get from them.

https://www.kaggle.com/sonujha090/insurance-prediction

## Attribute Information:

- age integer
- sex binary
- bmi (body mass index) float
- children integer
- smoker binary
- region categorial
- charges float
